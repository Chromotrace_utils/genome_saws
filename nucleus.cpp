//
// Created by Tomas Fitzgerald on 23/09/2016.
//

#include "nucleus.h"
#include <stdlib.h>
#include <math.h>
#include <iostream>

void nucleus::generate_model() {
    int x, y, z;
    int point;

    int nucleus_diameter = params.getNucleus_diameter();
    int nucleus_radius = params.getNucleus_radius();
    int nucleolus_radius = params.getNucleolus_radius();

    /* initialize the nuclues model matrix */
    grid = (int ***) malloc(sizeof(int **) * nucleus_diameter);
    for(z=0; z<nucleus_diameter; z++){
        grid[z] = (int **) malloc(sizeof(int *) * nucleus_diameter);
        for(y=0; y<nucleus_diameter; y++){
            grid[z][y] = (int *) malloc(sizeof(int ) * nucleus_diameter);
            for(x=0; x<nucleus_diameter; x++){
                point = pow((x-nucleus_radius),2) + pow((y-nucleus_radius),2) + pow((z-nucleus_radius),2);
                /* Check if the point belongs to a valid position inside nucleus and outside nucleolus */
                if(point<=pow(nucleus_radius,2) && point>pow(nucleolus_radius,2)){
                    grid[z][y][x] = 1;
                    line_length++;
                }
                else{
                    grid[z][y][x] = 0;
                }
            }
        }
    }
    printf("\tNucleus Model Generated\n");
}
