//
// Created by Tomas Fitzgerald on 23/09/2016.
//

#include "parameters.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void parameters::read_config_file(const char * filename) {

    int index = 0;
    char line[MAX_INPUT_LINE];
    char *elem, *brkt;
    int i;

    num_chrs = 0;
    num_sex_chrs = 0;
    FILE *file = fopen(filename, "r");

    while ((fgets(line, sizeof(line), file)) != 0) {
        index++;
        elem = strtok(line, "\t");
        if(index<=5){
            switch (index) {
                case 1:
                    elem = strtok(NULL, "\t");
                    nucleus_diameter = atoi(elem);
                    nucleus_radius = nucleus_diameter/2;
                    break;
                case 2:
                    elem = strtok(NULL, "\t");
                    nucleolus_diameter = atoi(elem);
                    nucleolus_radius = nucleolus_diameter/2;
                    break;
                case 3:
                    elem = strtok(NULL, "\t");
                    chromatin_density = atoi(elem);
                    break;
                case 4:
                    elem = strtok(NULL, "\t");
                    num_chrs = atoi(elem);
                    chrs_size = (int*) malloc(sizeof(int) * num_chrs);
                    for(i=0; i<num_chrs; i++){
                        chrs_size[i] = 0;
                    }
                    break;
                case 5:
                    elem = strtok(NULL, "\t");
                    num_sex_chrs = atoi(elem);
                    sex_chrs_size = (int*) malloc(sizeof(int) * num_sex_chrs);
                    for(i=0; i<num_sex_chrs; i++){
                        sex_chrs_size[i] = 0;
                    }
                    break;
            }
        }
        if(index>5){
            if(index<=(5+num_chrs)){
                elem = strtok(NULL, "\t");
                chrs_size[index-5-1] = (int) atoi(elem)/chromatin_density;
            }
            if(index>(5+num_chrs)){
                elem = strtok(NULL, "\t");
                sex_chrs_size[index-5-num_chrs-1] = (int) atoi(elem)/chromatin_density;
            }
        }
    }
    fclose(file);
    printf("\tParameters Correctly Loaded:\n");
    printf("\t - Nucleus Diamater\t%d (radius %d)\n",nucleus_diameter, nucleus_radius);
    printf("\t - Nucleolus Diamater\t%d (radius %d)\n",nucleolus_diameter, nucleolus_radius);
    printf("\t - Chromatin Density\t%d\n",chromatin_density);
    printf("\t - Number Chromosomes\t%d\n",num_chrs);
    printf("\t - Number Sex Chromosomes\t%d\n",num_sex_chrs);
    printf("\n");
}