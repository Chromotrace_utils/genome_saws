//
// Created by Tomas Fitzgerald on 23/09/2016.
//
#include "point.h"
#include "nucleus.h"
#include "parameters.h"
#ifndef GENOME_SAWS_SAW_H
#define GENOME_SAWS_SAW_H
#define MAX_SAW_ITER 3000
#define NUM_MOVE 6

class genome_saw {
    nucleus space;
    point **points;
    int curr_index;
    int curr_iteration;
    const char * out_file;

public:

    genome_saw(const nucleus &nuclear_space, const char *out_file) : space(nuclear_space), out_file(out_file) {
        execute();
    }

private:
    void initialize();

    void select_new_pivot(int action);

    int is_self_avoiding();

    void select_next_point();

    int getrandom(int i);

    void execute();

    void print_path(int index, int rep);
};


#endif //GENOME_SAWS_SAW_H
