# Genome SAWs

Code to generate test data for chromotrace. A number of self avoiding walks are generated in a sphere to represent a genome in a cell.

CMake must already be installed. This can be found at [CMake](https://cmake.org/)

## Install and Usage Instructions

Genome SAWs can be built be running the following and a binary named chromotrace_new will be built.. 

```
git clone git@gitlab.com:Chromotrace_utils/genome_saws.git
cd genome_saws
mkdir build
cd build
cmake ..
make
```

The code can be run with the following commands.

```
./genome_saws <path to config file> <output filepath>
```

## Config File Example

Genome SAWs takes a config file as argument, this must contain a number of parameters used in generating the simulated genome.
The format for the config file is given below.

```
Nucleus Diamater [nm]   <int>
Nucleolus Diamater [nm] <int>
Chromatin Density [bp/nm^3]     <int>
Number Chromosomes      <int>
Number Sex Chromosome   <int>
Size Chr1 [bp]  <int>
Size Chr2 [bp]  <int>
.
.
.
Size Chrm [bp]  <int> 
```

Below is an example of a config file.

```
Nucleus Diamater [nm]   150
Nucleolus Diamater [nm] 70
Chromatin Density [bp/nm^3]     12000
Number Chromosomes      22
Number Sex Chromosome   2
Size Chr1 [bp]  247199719
Size Chr2 [bp]  243315028
Size Chr3 [bp]  199411731
Size Chr4 [bp]  191610523
Size Chr5 [bp]  180967295
Size Chr6 [bp]  170740541
Size Chr7 [bp]  158431299
Size Chr8 [bp]  145908738
Size Chr9 [bp]  134505819
Size Chr10 [bp] 135480874
Size Chr11 [bp] 134978784
Size Chr12 [bp] 133464434
Size Chr13 [bp] 114151656
Size Chr14 [bp] 105311216
Size Chr15 [bp] 100114055
Size Chr16 [bp] 89995999
Size Chr17 [bp] 81691216
Size Chr18 [bp] 77753510
Size Chr19 [bp] 63790860
Size Chr20 [bp] 63644868
Size Chr21 [bp] 46976537
Size Chr22 [bp] 49476972
Size ChrX [bp]  152634166
Size ChrY [bp]  5096109
```
