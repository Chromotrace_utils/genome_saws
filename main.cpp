#include <iostream>
#include "nucleus.h"
#include "genome_saw.h"

using namespace std;

int main(int argc, char* argv[]) {

    const char * config_file;
    const char * outputfile;

    if (argc < 3) {
        config_file = "/Users/tomas/ClionProjects/genome_saws/resources/saw_config.conf";
        outputfile = "/Users/tomas/ClionProjects/genome_saws/resources/out.out";
    } else {
        config_file = argv[1];
        outputfile = argv[2];
    }

    printf("STEP 1: Reading the Model Parameters Contained in the Configuration File\n");
    parameters params(config_file);

    printf("\nSTEP 2: Generating Model\n");
    nucleus space(params);

    printf("\nSTEP 2: Generating Genome SAWs\n");
    genome_saw saw(space, outputfile);

    return 0;
}

