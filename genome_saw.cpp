//
// Created by Tomas Fitzgerald on 23/09/2016.
//

#include "genome_saw.h"
#include "point.h"
#include <time.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>

void genome_saw::initialize(){
    int i, j, k;
    int x, y, z;

    points = ( point **) malloc(sizeof( point *) * space.getLine_length());
    for(i=0; i<space.getLine_length(); i++){
        points[i] = (struct point *) malloc(sizeof( point));
        points[i]->setX(-1);
        points[i]->setY(-1);
        points[i]->setZ(-1);
    }

    srand(time(NULL));
    x = (int) rand()%space.getParams().getNucleus_diameter();
    y = (int) rand()%space.getParams().getNucleus_diameter();
    z = (int) rand()%space.getParams().getNucleus_diameter();
    while(space.grid[z][y][x]==0){
        srand(time(NULL));
        x = (int) rand()%space.getParams().getNucleus_diameter();
        y = (int) rand()%space.getParams().getNucleus_diameter();
        z = (int) rand()%space.getParams().getNucleus_diameter();
    }

    points[0]->setX(x);
    points[0]->setY(y);
    points[0]->setZ(z);

    curr_index = 0;
    space.grid[z][y][x] = 0;

}

void genome_saw::select_new_pivot(int action){
    int i,j, index_new_pivot, index;
    int x, y,z;

    index = curr_index;
    index_new_pivot = getrandom((int) (curr_index));
    index_new_pivot = (int) curr_index - (index_new_pivot/20)-1;

    if(index_new_pivot<0){
        index_new_pivot=0;
    }
    if(action==1){
        index_new_pivot=0;
    }
    for(i=(index_new_pivot+1);i<=index;i++){
        x = points[i]->getX();
        y = points[i]->getY();
        z = points[i]->getZ();
        space.grid[z][y][x] = 1;
    }
    if(action==1){
        initialize();
    }else{
        curr_index=index_new_pivot;
    }
}

int genome_saw::is_self_avoiding(){
    int currx, curry, currz, x, y,z;
    int is_saw=0;
    currx = points[curr_index]->getX();
    curry = points[curr_index]->getY();
    currz = points[curr_index]->getZ();

    int nucleus_diameter = space.getParams().getNucleus_diameter();
    for(z=(currz-1); z<=(currz+1); z++){
        if(z>=0 && z<nucleus_diameter && space.grid[z][curry][currx]==1){
            is_saw=1;
        }
    }

    for(y=(curry-1); y<=(curry+1); y++){
        if(y>=0 && y<nucleus_diameter && space.grid[currz][y][currx]==1){
            is_saw=1;
        }
    }

    for(x=(currx-1); x<=(currx+1); x++){
        if(x>=0 && x<nucleus_diameter && space.grid[currz][curry][x]==1){
            is_saw=1;
        }
    }
    return(is_saw);
}

void genome_saw::select_next_point(){
    int rand_move;
    int isvalid = 0;
    int x, y, z, nextx, nexty, nextz;
    point *tmp_point;

    x = points[curr_index]->getX();
    y = points[curr_index]->getY();
    z = points[curr_index]->getZ();
    nextx = x;
    nexty = y;
    nextz = z;

    rand_move = getrandom(NUM_MOVE);
    int xx, yy, zz;

    int nucleus_diameter = space.getParams().getNucleus_diameter();
    int i,j,k;
    for(i=0; i<nucleus_diameter; i++){
        for(j=0; j<nucleus_diameter; j++){
            for(k=0; k<nucleus_diameter; k++){
                //printf("curr(z,y,x)=%d,%d,%d - adj(z,y,x)=%d,%d,%d -> %d\n",z, y, x, i, j, k, grid[i][j][k]);
            }
        }
    }
    xx=0;
    while(isvalid==0){
        xx++;
        if(rand_move==0 && (z-1)>=0 && space.grid[(z-1)][y][x]==1){
            isvalid=1;
            nextz--;
            space.grid[nextz][y][x] =0;
        }
        if(rand_move==1 && (y-1)>=0 && space.grid[z][(y-1)][x]==1){
            isvalid=1;
            nexty--;
            space.grid[z][nexty][x] =0;
        }
        else if(rand_move==2 && (x+1)<nucleus_diameter && space.grid[z][y][(x+1)]==1){
            isvalid=1;
            nextx++;
            space.grid[z][y][nextx] =0;
        }
        else if(rand_move==3 && (y+1)<nucleus_diameter && space.grid[z][(y+1)][x]==1){
            isvalid=1;
            nexty++;
            space.grid[z][nexty][x] =0;
        }
        else if(rand_move==4 && (x-1)>=0 && space.grid[z][y][(x-1)]==1){
            isvalid=1;
            nextx--;
            space.grid[z][y][nextx] =0;
        }
        else if(rand_move==5 && (z+1)<nucleus_diameter && space.grid[(z+1)][y][x]==1){
            isvalid=1;
            nextz++;
            space.grid[nextz][y][x] =0;
        }
        if(isvalid==0){
            rand_move++;
            if(rand_move > (NUM_MOVE-1)){
                rand_move=0;
            }
        }
    }

    curr_index++;
    points[curr_index]->setX(nextx);
    points[curr_index]->setY(nexty);
    points[curr_index]->setZ(nextz);
}

int genome_saw::getrandom(int i){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return((int) tv.tv_usec%i);
}

void genome_saw::print_path(int index, int rep){
    int i;
    point *current_point, *next_point;
    int line_length = space.getLine_length();
    FILE *file = fopen(out_file, "ab");
    for(i=0;i<line_length;i++){
        fprintf(file, "\nchr%d.%d\t%d\t%d\t%d", index+1, rep, points[i]->getX(), points[i]->getY(), points[i]->getZ());

    }
    fclose(file);
}

void genome_saw::execute () {

    int i, j;
    int is_saw;
    int max_iter;
    int n_iter;
    int tot_iter;

    int line_length = space.getLine_length();
    int num_chrs = space.getParams().getNum_chrs();
    int * chrs_size = space.getParams().getChrs_size();
    int num_sex_chrs = space.getParams().getNum_sex_chrs();
    int * sex_chrs_size = space.getParams().getSex_chrs_size();

    printf("\n\nSTEP 3: Generating SAWs\n");
    for(i=0; i<(2*num_chrs);i++){
        curr_iteration = (int) i/2;
        tot_iter = 0;
        printf("\tSAW for chromosome %d rep %d\n",curr_iteration+1, (i%2)+1);
        line_length = chrs_size[curr_iteration];
        initialize();
        while(curr_index<line_length){
            //printf("\t chr %d rep %d > line length %d of %d\n", curr_iteration+1, (i%2)+1, curr_index, line_length);
            is_saw = is_self_avoiding();
            if(curr_index==(line_length-1)){
                break;
            }else{
                if(is_saw==1){
                    select_next_point();
                }
                else{
                    tot_iter++;
                    if(tot_iter<MAX_SAW_ITER){
                        printf("\t>No self avoiding length line = %d of %d \n", curr_index, line_length);
                        select_new_pivot(0);
                    }else{
                        tot_iter=0;
                        printf("\t>Initialize No self avoiding length line = %d of %d \n", curr_index, line_length);
                        select_new_pivot(1);
                    }
                }
            }
        }
        printf("... Computed\n");
        print_path(curr_iteration, (i%2)+1);

    }

    j=22;
    for(i=0; i<num_sex_chrs;i++){
        curr_iteration = i;
        //printf("\tSAW for sex chromosome %d",j);
        line_length = sex_chrs_size[curr_iteration];
        initialize();
        tot_iter=0;
        while(curr_index<line_length){
            printf("\t chr %d rep %d > line length %d of %d\n", curr_iteration+1, (i%2)+1, curr_index, line_length);
            is_saw = is_self_avoiding();
            if(curr_index==(line_length-1)){
                break;
            }else{
                if(is_saw==1){
                    select_next_point();
                }
                else{
                    tot_iter++;
                    if(tot_iter<MAX_SAW_ITER){
                        printf("\t>No self avoiding length line = %d of %d \n", curr_index, line_length);
                        select_new_pivot(0);
                    }else{
                        tot_iter=0;
                        printf("\t>Initialize No self avoiding length line = %d of %d \n", curr_index, line_length);
                        select_new_pivot(1);
                    }
                }
            }
        }
        printf("... Computed\n");
        print_path(j, i+1);
        j++;

    }
}
