//
// Created by Tomas Fitzgerald on 23/09/2016.
//

#ifndef GENOME_SAWS_POINT_H
#define GENOME_SAWS_POINT_H


class point {
    int x;
    int y;
    int z;

public:
    int getX() const {
        return x;
    }

    void setX(int x) {
        point::x = x;
    }

    int getY() const {
        return y;
    }

    void setY(int y) {
        point::y = y;
    }

    int getZ() const {
        return z;
    }

    void setZ(int z) {
        point::z = z;
    }

public:
    point(int z, int x, int y) : z(z), x(x), y(y) { }
};


#endif //GENOME_SAWS_POINT_H
