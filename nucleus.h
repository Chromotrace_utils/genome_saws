//
// Created by Tomas Fitzgerald on 23/09/2016.
//
#include "parameters.h"
#ifndef GENOME_SAWS_NUCLEUS_H
#define GENOME_SAWS_NUCLEUS_H

class nucleus {
    parameters params;
    int line_length;

public:
    int ***grid;

    nucleus(parameters params): params(params) {
        generate_model();
    }

    int getLine_length() const {
        return line_length;
    }

    void setLine_length(int line_length) {
        nucleus::line_length = line_length;
    }

    const parameters &getParams() const {
        return params;
    }

    void setParams(const parameters &params) {
        nucleus::params = params;
    }

    void generate_model();
};


#endif //GENOME_SAWS_NUCLEUS_H
