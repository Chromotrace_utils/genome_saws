//
// Created by Tomas Fitzgerald on 23/09/2016.
//

#ifndef GENOME_SAWS_PARAMS_H
#define GENOME_SAWS_PARAMS_H
#define MAX_INPUT_LINE 1024

class parameters {
    int nucleus_diameter;// = 600;
    int nucleus_radius;// = (int) 600/2;
    int nucleolus_diameter;// = 10;
    int nucleolus_radius;// = (int) 10/2;
    int chromatin_density;
    int num_chrs;
    int num_sex_chrs;
    int *chrs_size;
    int *sex_chrs_size;

public:
    parameters(const char * filename) {
        read_config_file(filename);
    }

    int getNucleus_diameter() const {
        return nucleus_diameter;
    }

    void setNucleus_diameter(int nucleus_diameter) {
        parameters::nucleus_diameter = nucleus_diameter;
    }

    int getNucleus_radius() const {
        return nucleus_radius;
    }

    void setNucleus_radius(int nucleus_radius) {
        parameters::nucleus_radius = nucleus_radius;
    }

    int getNucleolus_diameter() const {
        return nucleolus_diameter;
    }

    void setNucleolus_diameter(int nucleolus_diameter) {
        parameters::nucleolus_diameter = nucleolus_diameter;
    }

    int getNucleolus_radius() const {
        return nucleolus_radius;
    }

    void setNucleolus_radius(int nucleolus_radius) {
        parameters::nucleolus_radius = nucleolus_radius;
    }

    int getChromatin_density() const {
        return chromatin_density;
    }

    void setChromatin_density(int chromatin_density) {
        parameters::chromatin_density = chromatin_density;
    }

    int getNum_chrs() const {
        return num_chrs;
    }

    void setNum_chrs(int num_chrs) {
        parameters::num_chrs = num_chrs;
    }

    int getNum_sex_chrs() const {
        return num_sex_chrs;
    }

    void setNum_sex_chrs(int num_sex_chrs) {
        parameters::num_sex_chrs = num_sex_chrs;
    }

    int *getChrs_size() const {
        return chrs_size;
    }

    void setChrs_size(int *chrs_size) {
        parameters::chrs_size = chrs_size;
    }

    int *getSex_chrs_size() const {
        return sex_chrs_size;
    }

    void setSex_chrs_size(int *sex_chrs_size) {
        parameters::sex_chrs_size = sex_chrs_size;
    }

    void read_config_file(const char * filename);

};


#endif //GENOME_SAWS_PARAMS_H
